use chrono::{DateTime, Local};
use crypto_hash::{Algorithm, hex_digest};
use std::io::Write;
use serde::{Serialize, Deserialize};
use std::env;
use std::fs::File;
use std::fmt;
use std::fs;
use std::io;
use std::path::Path;
use std::path::PathBuf;
use std::time::SystemTime;

use mime_guess::from_path;


fn main() {
    let _index_indicator = env::args().nth(1).expect("State your intentions!");
    let directory_name = env::args().nth(2).expect("No directory given.");

    let mut file_paths: Vec<PathBuf> = Vec::new();
    let dir_name = Path::new(&directory_name);

    if let Err(err) = find_all_files(&dir_name, &mut file_paths) {
        eprintln!("Error: {}", err);
    } else {
        let baf_file_path = dir_name.join(".baf");
        let mut baf_file = File::create(&baf_file_path).expect("Failed to create .baf file");

        for path in file_paths {
            print!("{}\n", path.display());

            if let Ok(current_file_info) = get_file_info(&path) {
                // Write file information to the .baf file
                writeln!(baf_file, "{:?}", current_file_info).expect("Failed to write to .baf file");
            } else {
                eprintln!("Error processing file {}", path.display());
            }
            /* match get_file_info(&path) {
                Ok(current_file_info) => {
                    println!("{:?}", current_file_info);
                }
                Err(err) => {
                    eprintln!("Error processing file {}: {}", path.display(), err);
                }
            }*/
        }
    }
}

fn find_all_files(dir: &Path, paths: &mut Vec<PathBuf>) -> std::io::Result<()> {
    if dir.is_dir() {

        /* For now we just erase the already existing .baf file, the code needs to implement
         a function to just modify it when necessary */
        let baf_file_path = dir.join(".baf");

        if baf_file_path.exists() {
            if let Err(err) = fs::remove_file(&baf_file_path) {
                eprintln!("Failed to remove .baf file: {}", err);
            }
        }

        for entry in fs::read_dir(dir)? {
            let path = entry?.path();

            if path.is_dir() {
                find_all_files(&path, paths)?;
            } else {
                paths.push(path);
            }
        }
    }
    Ok(())
}

fn get_file_info(file_path: &PathBuf) -> io::Result<FileInfo> {

    let metadata = fs::metadata(file_path)?;
    let file_content = fs::read(file_path)?;
    let sha256 = hex_digest(Algorithm::SHA256, &file_content);
    let file_type = from_path(Path::new(file_path)).first_or_octet_stream().to_string();

    let creation_time = metadata.created().ok().map(|time| format_time(time));
    let modification_time = format_time(metadata.modified()?);

    Ok(FileInfo {
        sha256,
        file_type,
        creation_time,
        modification_time,
        file_size: metadata.len()
    })
}

fn format_time(time: SystemTime) -> String {
    let datetime: DateTime<Local> = time.into();
    datetime.format("%Y-%m-%d %H:%M:%S").to_string()
}

#[derive(Serialize, Deserialize)]
struct FileInfo {
    sha256: String,
    file_type: String,
    creation_time: Option<String>,
    modification_time: String,
    file_size: u64,
}

// This is to print the information for various tests
impl fmt::Debug for FileInfo {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "FileInfo {{\n  SHA256: {}\n  File type: {}\n  Creation time: {:?}\n  Modification time: {:?}\n  File size: {} bytes\n}}",
            self.sha256, self.file_type, self.creation_time, self.modification_time, self.file_size
        )
    }
}